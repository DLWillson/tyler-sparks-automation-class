variable "project" {
  default = "auto-dev"
}

variable "region" {
  default = "us-west-2"
}

variable "spot_price" {
  default = 0.1
}

variable "domain_name" {
  default = "example.com"
}
