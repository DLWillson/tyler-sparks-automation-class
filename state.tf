provider "aws" {
  region = "${var.region}"
}

# bucket to provision for remote state
resource "aws_s3_bucket" "remote-state" {
  bucket        = "terraform-${var.project}-dlwillson"
  acl           = "private"
  force_destroy = true

  versioning {
    enabled = true
  }
}

# test instance to validate permissions
resource "aws_instance" "test" {
  ami            = "ami-076e276d85f524150"
  instance_type  = "t2.micro"
}


